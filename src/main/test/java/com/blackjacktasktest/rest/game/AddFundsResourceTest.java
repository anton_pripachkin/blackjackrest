package com.blackjacktasktest.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by apripachkin on 11/20/15.
 */
public class AddFundsResourceTest extends AbstractResourceTest {

    @Test
    public void testAddFunds() throws Exception {
        GameState post = target(Constants.PATH_ADD_FUNDS).path("20").request().post(null, GameState.class);
        assertEquals(Constants.INITIALIZE_GAME, post.getGameStatus());
    }
}