package com.blackjacktasktest.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;
import com.sun.tools.internal.jxc.ap.Const;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.MediaType;

import static org.junit.Assert.*;

/**
 * Created by apripachkin on 11/20/15.
 */
public class CreateAccountResourceTest extends AbstractResourceTest {

    @Test
    public void testCreateGameUser() throws Exception {
        Invocation.Builder request = target(Constants.PATH_CREATE_ACCOUNT).request();
        GameState post = request.post(Entity.entity(createUserRequest, MediaType.APPLICATION_JSON_TYPE), GameState.class);
        assertEquals(createUserRequest.getBalance(), post.getUserBalance(), 0);
    }

    @Test
    public void testWrondUserName() throws Exception {
        Invocation.Builder request = target(Constants.PATH_CREATE_ACCOUNT).request();
        createUserRequest.setUserName(null);
        GameState post = request.post(Entity.entity(createUserRequest, MediaType.APPLICATION_JSON_TYPE), GameState.class);
        assertEquals(Constants.METHOD_PARAMETER_NULL_ERROR, post.getGameStatus());
    }

    @Test
    public void testWrongBalance() throws Exception {
        Invocation.Builder request = target(Constants.PATH_CREATE_ACCOUNT).request();
        createUserRequest.setBalance(-1);
        GameState post = request.post(Entity.entity(createUserRequest, MediaType.APPLICATION_JSON_TYPE), GameState.class);
        assertEquals(Constants.WRONG_BALANCE_VALUE, post.getGameStatus());
    }
}