package com.blackjacktasktest.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by apripachkin on 11/20/15.
 */
public class StartGameResourceTest extends AbstractResourceTest {

    @Test
    public void testStartGame() throws Exception {
        GameState post = target(Constants.PATH_START_GAME).path("20").request().post(null, GameState.class);
        assertEquals(Constants.GAME_STARTED, post.getGameStatus());
    }
}