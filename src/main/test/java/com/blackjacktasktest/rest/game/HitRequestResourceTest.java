package com.blackjacktasktest.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anton Pripachkin on 19.11.2015.
 */

public class HitRequestResourceTest extends AbstractResourceTest{

    @Test
    public void testUninitializedGame() {
        GameState hit = target(Constants.PATH_HIT).request().post(null, GameState.class);
        assertEquals(Constants.INITIALIZE_GAME, hit.getGameStatus());
    }

}
