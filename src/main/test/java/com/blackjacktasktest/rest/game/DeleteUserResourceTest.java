package com.blackjacktasktest.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by apripachkin on 11/20/15.
 */
public class DeleteUserResourceTest extends AbstractResourceTest {

    @Test
    public void testDeleteUser() throws Exception {
        GameState gameState = target(Constants.PATH_DELETE).path(createUserRequest.getUserName()).request().post(null, GameState.class);
        assertEquals(Constants.DELETE_ACTIVE_USER, gameState.getGameStatus());
    }
}