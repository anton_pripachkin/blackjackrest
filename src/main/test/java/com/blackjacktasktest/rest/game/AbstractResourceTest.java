package com.blackjacktasktest.rest.game;

import com.blackjacktask.rest.model.requests.CreateUserRequest;
import com.blackjacktask.rest.session.impl.GameSession;
import com.blackjacktask.utilities.Constants;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;

import javax.ws.rs.core.Application;

/**
 * Created by apripachkin on 11/20/15.
 */
public abstract class AbstractResourceTest extends JerseyTest {

    public static final String DEFAULT_PACKAGE = "com.blackjacktask.rest.game.";
    protected CreateUserRequest createUserRequest;
    protected GameSession session;

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
        session = new GameSession();
        createUserRequest = new CreateUserRequest();
        createUserRequest.setUserName(Constants.TEST_USERNAME);
        createUserRequest.setBalance(200);
    }

    @Override
    @After
    public void tearDown() throws Exception {
        super.tearDown();
        session = null;
        createUserRequest = null;
    }

    @Override
    protected Application configure() {
        String className = getClass().getSimpleName();
        String testClassName = DEFAULT_PACKAGE + className.substring(0, className.indexOf("Test"));
        Class<?> testClass = null;
        try {
            testClass = Class.forName(testClassName);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new ResourceConfig(testClass);
    }
}
