package com.blackjacktasktest.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by apripachkin on 11/20/15.
 */
public class StandRequestResourceTest extends AbstractResourceTest {

    @Test
    public void testStandRequest() throws Exception {
        GameState post = target(Constants.PATH_STAND).request().post(null, GameState.class);
        assertEquals(Constants.INITIALIZE_GAME, post.getGameStatus());
    }
}