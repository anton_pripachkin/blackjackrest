package com.blackjacktasktest.rest.validator;

import com.blackjacktask.rest.session.validator.GameSessionValidator;
import com.blackjacktask.rest.session.validator.impl.AddFundsValidator;
import com.blackjacktask.rest.session.validator.impl.HitOrStandValidator;
import com.blackjacktask.rest.session.validator.impl.InitGameValidator;
import com.blackjacktask.utilities.Constants;
import org.junit.After;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by apripachkin on 11/19/15.
 */
public class GameSessionValidatorTest {
    private GameSessionValidator gameSessionValidator;

    @After
    public void tearDown() throws Exception {
        gameSessionValidator = null;
    }

    @Test(expected = IllegalStateException.class)
    public void testValidateFunds() {
        gameSessionValidator = new AddFundsValidator();
        gameSessionValidator.validateData(-1l, (double) 20);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateBalance() {
        gameSessionValidator = new AddFundsValidator();
        gameSessionValidator.validateData(1l, (double) -20);
    }

    @Test(expected = IllegalStateException.class)
    public void testHitValidate() {
        gameSessionValidator = new HitOrStandValidator();
        gameSessionValidator.validateData(null, null);
    }

    @Test(expected = IllegalStateException.class)
    public void testStandValidate() {
        gameSessionValidator = new HitOrStandValidator();
        ArrayList<String> actions = new ArrayList<>();
        actions.add(Constants.ACTION_HIT);
        gameSessionValidator.validateData(actions, Constants.ACTION_STAND);
    }

    @Test (expected = IllegalStateException.class)
    public void testInitGameAlreadyRunning() {
        gameSessionValidator = new InitGameValidator();
        ArrayList<String> actions = new ArrayList<>();
        actions.add(Constants.ACTION_HIT);
        gameSessionValidator.validateData( 1l, actions);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testInitGameWrongAction() {
        gameSessionValidator = new InitGameValidator();
        ArrayList<String> actions = new ArrayList<>();
        actions.add(Constants.ACTION_STAND);
        gameSessionValidator.validateData(-1l, actions);
    }

}