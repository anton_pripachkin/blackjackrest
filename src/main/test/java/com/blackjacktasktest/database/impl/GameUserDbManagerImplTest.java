package com.blackjacktasktest.database.impl;

import com.blackjacktask.database.dao.GameUser;
import com.blackjacktask.database.impl.GameUserDbManagerImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public class GameUserDbManagerImplTest {
    private GameUserDbManagerImpl gameUserDbManagerImpl;
    public static final String USER_NAME = "Test";

    @Before
    public void setUp() throws Exception {
        gameUserDbManagerImpl = new GameUserDbManagerImpl();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println(gameUserDbManagerImpl.removeAllEntries());
        gameUserDbManagerImpl = null;
    }

    @Test
    public void testSaveUser() throws Exception {
        GameUser gameUser = gameUserDbManagerImpl.saveUser(USER_NAME, 100);
        assertEquals(gameUser, gameUserDbManagerImpl.getUser(gameUser.getId()));
    }

    @Test
    public void testUpdateBalance() throws Exception {
        GameUser gameUser = gameUserDbManagerImpl.saveUser(USER_NAME, 100);
        gameUserDbManagerImpl.updateBalance(gameUser.getId(), -100);
        assertEquals(-100, gameUser.getBalance(), 0);
    }

    @Test
    public void testIsUserPlaying() throws Exception {
        GameUser gameUser = gameUserDbManagerImpl.saveUser(USER_NAME, 100);
        assertFalse(gameUser.getIsPlaying());
        gameUser.setIsPlaying(true);
        assertTrue(gameUserDbManagerImpl.isUserPlaying(gameUser.getId()));
    }

    @Test
    public void testGetUsers() throws Exception {
        gameUserDbManagerImpl.saveUser(USER_NAME, 100);
        gameUserDbManagerImpl.saveUser(USER_NAME + "1", 100);
        List<GameUser> users = gameUserDbManagerImpl.getUsers();
        assertTrue(users.size() == 2);
    }


    @Test
    public void testDeleteUser() throws Exception {
        GameUser gameUser = gameUserDbManagerImpl.saveUser(USER_NAME, 100);
        gameUserDbManagerImpl.deleteUser(gameUser.getId());
        assertTrue(gameUserDbManagerImpl.getUsers().size() == 0);
    }
}