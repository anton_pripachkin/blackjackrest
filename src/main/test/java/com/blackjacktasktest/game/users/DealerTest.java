package com.blackjacktasktest.game.users;

import com.blackjacktask.game.cards.Card;
import com.blackjacktask.game.cards.CardSuit;
import com.blackjacktask.game.users.Dealer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Anton Pripachkin on 14.11.2015.
 */
public class DealerTest {
    private Dealer dealer;

    @Before
    public void setUp() throws Exception {
        dealer = new Dealer();
    }

    @After
    public void tearDown() throws Exception {
        dealer = null;
    }

    @Test
    public void testWillHit() throws Exception {
        assertTrue(dealer.willHit());
        dealer.takeCard(new Card(1, CardSuit.Clubs));
        dealer.takeCard(new Card(10, CardSuit.Clubs));
        assertFalse(dealer.willHit());
    }

    @Test
    public void testShowDealerCards() throws Exception {
        dealer.takeCard(new Card(1, CardSuit.Clubs));
        final List<Card> hiddenCard = dealer.showDealerCards();
        assertEquals(0, hiddenCard.size());
        final List<Card> notHiddenCard = dealer.getGamePlayerCards();
        assertTrue(hiddenCard.size() < notHiddenCard.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTakeCard() throws Exception {
        dealer.takeCard(null);
    }

    @Test
    public void testGetGamePlayerCards() throws Exception {
        final int zeroSize = dealer.getGamePlayerCards().size();
        dealer.takeCard(new Card(1, CardSuit.Clubs));
        final int biggerSize = dealer.getGamePlayerCards().size();
        assertTrue(biggerSize > zeroSize);
    }

    @Test
    public void testResetCards() throws Exception {
        dealer.takeCard(new Card(1, CardSuit.Clubs));
        final int firstCardSum = dealer.getCardSum();
        dealer.resetCards();
        dealer.takeCard(new Card(10, CardSuit.Clubs));
        final int lowerCardSum = dealer.getCardSum();
        assertTrue(firstCardSum > lowerCardSum);
    }

    @Test
    public void testIsWinConditionMet() throws Exception {
        dealer.takeCard(new Card(1, CardSuit.Clubs));
        dealer.takeCard(new Card(10, CardSuit.Clubs));
        assertTrue(dealer.isWinConditionMet());
    }

    @Test
    public void testGetCardSum() throws Exception {
        dealer.takeCard(new Card(1, CardSuit.Clubs));
        dealer.takeCard(new Card(10, CardSuit.Clubs));
        assertEquals(21, dealer.getCardSum());
    }
}