package com.blackjacktasktest.game.cards;

import com.blackjacktask.game.cards.Card;
import com.blackjacktask.game.cards.CardSuit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anton Pripachkin on 12.11.2015.
 */
public class CardTest {
    private Card testCard;

    @Before
    public void setUp() {
        testCard = new Card();
    }
    @After
    public void tearDown() {
        testCard = null;
    }

    @Test
    public void testGetRank() throws Exception {
        testCard.setRank(1);
        assertEquals(1,testCard.getRank());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSetRank() throws Exception {
        testCard.setRank(-1);
    }

    @Test
    public void testGetCardSuit() throws Exception {
        testCard.setCardSuit(CardSuit.Clubs);
        assertEquals(CardSuit.Clubs, testCard.getCardSuit());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetCardSuit() {
        testCard.setCardSuit(null);
    }

}