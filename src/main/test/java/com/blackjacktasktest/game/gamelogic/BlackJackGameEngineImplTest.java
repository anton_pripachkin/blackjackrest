package com.blackjacktasktest.game.gamelogic;

import com.blackjacktask.database.dao.GameUser;
import com.blackjacktask.game.gamelogic.RoundResult;
import com.blackjacktask.game.gamelogic.impl.BlackJackGameEngineImpl;
import com.blackjacktask.rest.model.GameState;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by apripachkin on 11/19/15.
 */
public class BlackJackGameEngineImplTest {
    private BlackJackGameEngineImpl blackJackGameEngine;
    private GameUser gameUser;

    @Before
    public void setUp() throws Exception {
        blackJackGameEngine = new BlackJackGameEngineImpl();
        gameUser = initDummyUser();
    }

    @After
    public void tearDown() throws Exception {
        blackJackGameEngine = null;
        gameUser = null;
    }

    @Test
    public void testInitGame() throws Exception {
        GameState gameState = blackJackGameEngine.initGame(gameUser);
        assertEquals(100, gameState.getUserBalance(), 0);
    }

    private GameUser initDummyUser() {
        GameUser gameUser = new GameUser();
        gameUser.setBalance((double) 100);
        gameUser.setId((long) 1);
        gameUser.setIsPlaying(true);
        gameUser.setUserName("blabla");
        return gameUser;
    }

    @Test(expected = Exception.class)
    public void testHit() throws Exception {
        blackJackGameEngine.hit();
    }

    @Test(expected = Exception.class)
    public void testStand() throws Exception {
        blackJackGameEngine.stand();
    }

    @Test(expected = Exception.class)
    public void testStartGame() throws Exception {
        blackJackGameEngine.startGame(20);
    }

    @Test(expected = Exception.class)
    public void testUpdateBalance() throws Exception {
        blackJackGameEngine.updateBalance(20);
    }

    @Test
    public void checkBlackJack() {
        blackJackGameEngine.initGame(gameUser);
        GameState gameState = blackJackGameEngine.cheatPlayerBlackJack();
        RoundResult roundResult = gameState.getRoundResult();
        assertEquals(RoundResult.WIN, roundResult);
    }

    @Test
    public void checkPush() {
        blackJackGameEngine.initGame(gameUser);
        RoundResult roundResult = blackJackGameEngine.cheatPush().getRoundResult();
        assertEquals(RoundResult.DRAW, roundResult);
    }

    @Test
    public void checkDealerBlackJack() {
        blackJackGameEngine.initGame(gameUser);
        RoundResult roundResult = blackJackGameEngine.cheatDealerBlackJack().getRoundResult();
        assertEquals(RoundResult.LOSE, roundResult);
    }

    @Test
    public void testUserWinStand() {
        blackJackGameEngine.initGame(gameUser);
        blackJackGameEngine.cheatUserWinStand();
        RoundResult roundResult = blackJackGameEngine.stand().getRoundResult();
        assertEquals(RoundResult.WIN, roundResult);
    }

}