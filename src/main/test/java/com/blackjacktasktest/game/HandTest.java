package com.blackjacktasktest.game;

import com.blackjacktask.game.Hand;
import com.blackjacktask.game.cards.Card;
import com.blackjacktask.game.cards.CardSuit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by apripachkin on 11/13/15.
 */
public class HandTest {
    private Hand hand;

    @Before
    public void setUp() throws Exception {
        hand = new Hand();
    }

    @After
    public void tearDown() throws Exception {
        hand = null;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullCard() {
        boolean result = hand.takeCard(null);
        assertTrue(result);
    }

    @Test
    public void testGetHandPoints() {
        assertEquals(0, hand.getCurrentPoints());
        hand.takeCard(new Card(1, CardSuit.Clubs));
        assertEquals(11, hand.getCurrentPoints());
    }

    @Test
    public void testIsWinConditionMet() {
        assertFalse(hand.isWinConditionMet());
        hand.takeCard(new Card(1,CardSuit.Clubs));
        hand.takeCard(new Card(11,CardSuit.Clubs));
        assertTrue(hand.isWinConditionMet());
    }
}