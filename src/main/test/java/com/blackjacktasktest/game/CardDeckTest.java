package com.blackjacktasktest.game;

import com.blackjacktask.game.CardDeck;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Anton Pripachkin on 14.11.2015.
 */
public class CardDeckTest {
    private CardDeck cardDeck;

    @Before
    public void setUp() throws Exception {
        cardDeck = new CardDeck();
    }

    @After
    public void tearDown() throws Exception {
        cardDeck = null;
    }
    @Test
    public void testCardDeck() throws Exception {
        assertNotNull(cardDeck.getCard());
        cardDeck.resetCardDeck();
        assertNotNull(cardDeck.getCard());
    }
}