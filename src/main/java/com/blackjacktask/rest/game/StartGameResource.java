package com.blackjacktask.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by apripachkin on 11/16/15.
 */
@Path(Constants.PATH_START_GAME)
public class StartGameResource extends BaseGameResource {
    @POST
    @Path("/{bet}")
    @Produces(MediaType.APPLICATION_JSON)
    public GameState startGame(@PathParam("bet") double bet) {
        return session.startGame(bet);
    }
}
