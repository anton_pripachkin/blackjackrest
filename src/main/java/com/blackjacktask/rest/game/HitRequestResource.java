package com.blackjacktask.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
@Path(Constants.PATH_HIT)
public class HitRequestResource extends BaseGameResource{
    @POST
    public GameState hitRequest() {
        return session.hitRequest();
    }
}
