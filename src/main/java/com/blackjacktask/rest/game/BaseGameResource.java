package com.blackjacktask.rest.game;

import com.blackjacktask.rest.session.BlackJackGameSession;
import com.blackjacktask.rest.session.impl.GameSession;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public abstract class BaseGameResource {
    protected BlackJackGameSession session = GameSession.getInstance();
}
