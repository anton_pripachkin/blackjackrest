package com.blackjacktask.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Anton Pripachkin on 19.11.2015.
 */
@Path(Constants.PATH_DELETE)
public class DeleteUserResource extends BaseGameResource {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{username}")
    public GameState deleteUser(@PathParam("username") String userName) {
        return session.deleteUser(userName);
    }
}
