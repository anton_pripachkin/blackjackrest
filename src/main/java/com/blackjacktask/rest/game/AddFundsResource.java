package com.blackjacktask.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by apripachkin on 11/17/15.
 */
@Path(Constants.PATH_ADD_FUNDS)
public class AddFundsResource extends BaseGameResource {
    @POST
    @Path("/{amount}")
    @Produces(MediaType.APPLICATION_JSON)
    public GameState addFunds(@PathParam("amount") double amount) {
        return session.addFunds(amount);
    }
}
