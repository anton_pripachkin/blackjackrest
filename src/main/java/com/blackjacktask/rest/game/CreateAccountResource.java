package com.blackjacktask.rest.game;

import com.blackjacktask.rest.model.requests.CreateUserRequest;
import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;
import com.blackjacktask.utilities.Utils;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
@Path(Constants.PATH_CREATE_ACCOUNT)
public class CreateAccountResource extends BaseGameResource {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GameState createGameUser(CreateUserRequest createUserRequest) {
        try {
            String userName = createUserRequest.getUserName();
            Utils.checkForNull(Constants.METHOD_PARAMETER_NULL_ERROR, userName);
            Utils.checkBalanceValue(createUserRequest.getBalance());
            return session.initializeGame(createUserRequest);
        } catch (IllegalArgumentException e) {
            return Utils.handleGameError(e.getMessage());
        }
    }
}
