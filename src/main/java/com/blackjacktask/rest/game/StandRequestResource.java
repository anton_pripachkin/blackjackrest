package com.blackjacktask.rest.game;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
@Path(Constants.PATH_STAND)
public class StandRequestResource extends BaseGameResource{

    @POST
    public GameState standRequest() {
        return session.standRequest();
    }
}
