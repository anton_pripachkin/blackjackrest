package com.blackjacktask.rest.model.requests;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public class CreateUserRequest {
    private String userName;
    private double balance;

    public CreateUserRequest() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
