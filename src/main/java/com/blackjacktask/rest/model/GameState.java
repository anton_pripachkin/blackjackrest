package com.blackjacktask.rest.model;

import com.blackjacktask.game.cards.Card;
import com.blackjacktask.game.gamelogic.RoundResult;

import java.util.List;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public class GameState {
    private String gameStatus;
    private List<Card> userCards;
    private List<Card> dealerCards;
    private List<String> playerActions;
    private RoundResult roundResult;
    private double userBalance;

    public GameState () {}

    public String getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(String gameStatus) {
        this.gameStatus = gameStatus;
    }

    public List<Card> getUserCards() {
        return userCards;
    }

    public void setUserCards(List<Card> userCards) {
        this.userCards = userCards;
    }

    public List<Card> getDealerCards() {
        return dealerCards;
    }

    public void setDealerCards(List<Card> dealerCards) {
        this.dealerCards = dealerCards;
    }

    public List<String> getPlayerActions() {
        return playerActions;
    }

    public void setPlayerActions(List<String> playerActions) {
        this.playerActions = playerActions;
    }

    public RoundResult getRoundResult() {
        return roundResult;
    }

    public void setRoundResult(RoundResult roundResult) {
        this.roundResult = roundResult;
    }

    public double getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(double userBalance) {
        this.userBalance = userBalance;
    }

    @Override
    public String toString() {
        return "GameState{" +
                "gameStatus='" + gameStatus + '\'' +
                ", userCards=" + userCards +
                ", dealerCards=" + dealerCards +
                ", playerActions=" + playerActions +
                ", roundResult=" + roundResult +
                ", userBalance=" + userBalance +
                '}';
    }
}
