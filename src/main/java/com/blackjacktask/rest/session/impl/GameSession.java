package com.blackjacktask.rest.session.impl;

import com.blackjacktask.database.GameUserDbManager;
import com.blackjacktask.database.dao.GameUser;
import com.blackjacktask.database.impl.GameUserDbManagerImpl;
import com.blackjacktask.game.gamelogic.BlackJackGameEngine;
import com.blackjacktask.game.gamelogic.impl.BlackJackGameEngineImpl;
import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.rest.model.requests.CreateUserRequest;
import com.blackjacktask.rest.session.BlackJackGameSession;
import com.blackjacktask.rest.session.validator.GameSessionValidator;
import com.blackjacktask.rest.session.validator.impl.*;
import com.blackjacktask.utilities.Constants;
import com.blackjacktask.utilities.Utils;

import java.util.List;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public class GameSession implements BlackJackGameSession {
    private static GameSession instance;
    private GameUserDbManager dbManager = new GameUserDbManagerImpl();
    private BlackJackGameEngine blackJackGameEngine = new BlackJackGameEngineImpl();
    private long currentPlayerId = -1;
    private List<String> playerActions;

    private GameSessionValidator validator;

    public static GameSession getInstance() {
        if (instance == null) {
            instance = new GameSession();
        }
        return instance;
    }

    @Override
    public GameState initializeGame(CreateUserRequest createUserRequest) {
        try {
            validator = new InitGameValidator();
            validator.validateData(currentPlayerId, playerActions, createUserRequest);
            GameUser gameUser = dbManager.saveUser(createUserRequest.getUserName(), createUserRequest.getBalance());
            currentPlayerId = gameUser.getId();
            GameUser user = dbManager.getUser(currentPlayerId);
            user.setIsPlaying(true);
            return blackJackGameEngine.initGame(gameUser);
        } catch (Exception e) {
            return Utils.handleGameError(e.getMessage());
        }
    }

    @Override
    public GameState startGame(double bet) {
        try {
            validator = new StartGameValidator();
            validator.validateData(currentPlayerId, playerActions);
            return makeNewBet(bet);
        } catch (Exception e) {
            return Utils.handleGameError(e.getMessage());
        }
    }

    @Override
    public GameState hitRequest() {
        try {
            validator = new HitOrStandValidator();
            validator.validateData(playerActions, Constants.ACTION_HIT);
            GameState hitGameState = blackJackGameEngine.hit();
            checkGameState(hitGameState);
            return hitGameState;
        } catch (Exception e) {
            return Utils.handleGameError(e.getMessage());
        }
    }

    @Override
    public GameState standRequest() {
        try {
            validator = new HitOrStandValidator();
            validator.validateData(playerActions, Constants.ACTION_STAND);
            GameState standGameState = blackJackGameEngine.stand();
            checkGameState(standGameState);
            return standGameState;
        } catch (Exception e) {
            return Utils.handleGameError(e.getMessage());
        }
    }

    @Override
    public GameState addFunds(double amount) {
        try {
            validator = new AddFundsValidator();
            validator.validateData(currentPlayerId, amount);
            double updatedBalance = dbManager.getUser(currentPlayerId).getBalance() + amount;
            updateBalance(updatedBalance);
            GameState gameState = new GameState();
            gameState.setGameStatus(Constants.BALANCE_UPDATED);
            gameState.setUserBalance(updatedBalance);
            blackJackGameEngine.updateBalance(updatedBalance);
            return gameState;
        } catch (Exception e) {
            return Utils.handleGameError(e.getMessage());
        }
    }

    @Override
    public GameState deleteUser(String userName) {
        try {
            validator = new DeleteUserValidator();
            validator.validateData(currentPlayerId, dbManager.getUser(currentPlayerId));
            return deleteCurrentUser();
        } catch (Exception e) {
            return Utils.handleGameError(e.getMessage());
        }
    }

    private GameState deleteCurrentUser() {
        if (currentPlayerId != -1) {
            dbManager.deleteUser(currentPlayerId);
            blackJackGameEngine.resetBlackJackGame();
        }
        currentPlayerId = -1;
        playerActions = null;
        GameState gameState = new GameState();
        gameState.setGameStatus(Constants.USER_DELETED);
        return gameState;
    }

    private GameState makeNewBet(double bet) {
        if (Utils.checkBetValues(getUserBalance(), bet)) {
            GameState gameState = blackJackGameEngine.startGame(bet);
            playerActions = gameState.getPlayerActions();
            updateBalance(gameState.getUserBalance());
            return gameState;
        } else {
            return Utils.handleGameError(Constants.PLEASE_ADD_BALANCE);
        }
    }

    private void checkGameState(GameState currentGameState) {
        playerActions = currentGameState.getPlayerActions();
        if (playerActions.contains(Constants.START_NEW_ROUND)) {
            GameUser user = dbManager.getUser(currentPlayerId);
            user.setIsPlaying(false);
            updateBalance(currentGameState.getUserBalance());
            currentGameState.setGameStatus(Constants.NEED_TO_START_GAME);
        } else if (isUserPlaying()) {
            dbManager.getUser(currentPlayerId).setIsPlaying(true);
        }
    }

    private boolean isUserPlaying() {
        return playerActions.contains(Constants.ACTION_HIT) || playerActions.contains(Constants.ACTION_STAND);
    }

    private double getUserBalance() {
        GameUser user = dbManager.getUser(currentPlayerId);
        return user.getBalance();
    }

    private void updateBalance(double balance) {
        dbManager.updateBalance(currentPlayerId, balance);
    }
}