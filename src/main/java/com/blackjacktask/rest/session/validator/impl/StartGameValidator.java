package com.blackjacktask.rest.session.validator.impl;

import com.blackjacktask.rest.session.validator.GameSessionValidator;
import com.blackjacktask.utilities.Constants;

import java.util.List;

/**
 * Created by apripachkin on 11/18/15.
 */
public class StartGameValidator implements GameSessionValidator {
    @Override
    public void validateData(Object... objects) {
        long currentId = (long) objects[0];
        if (currentId == -1) {
            throw new IllegalStateException(Constants.INITIALIZE_GAME);
        }
        List<String> playerActions = (List<String>) objects[1];
        if (playerActions != null) {
            if (!playerActions.contains(Constants.START_NEW_ROUND)) {
                throw new IllegalArgumentException(Constants.HIT_OR_STAY);
            }
        }
    }
}
