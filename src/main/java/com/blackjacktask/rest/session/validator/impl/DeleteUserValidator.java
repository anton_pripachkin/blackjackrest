package com.blackjacktask.rest.session.validator.impl;

import com.blackjacktask.database.dao.GameUser;
import com.blackjacktask.rest.session.validator.GameSessionValidator;
import com.blackjacktask.utilities.Constants;

/**
 * Created by Anton Pripachkin on 19.11.2015.
 */
public class DeleteUserValidator implements GameSessionValidator {

    @Override
    public void validateData(Object... objects) {
        long currentId = (long) objects[0];
        if (currentId == -1) {
            throw new IllegalStateException(Constants.INITIALIZE_GAME);
        }
        GameUser currentUser = (GameUser) objects[1];
        if (currentUser.getIsPlaying()) {
            throw new IllegalStateException(Constants.DELETE_ACTIVE_USER);
        }
    }
}
