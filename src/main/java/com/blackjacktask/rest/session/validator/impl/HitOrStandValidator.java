package com.blackjacktask.rest.session.validator.impl;

import com.blackjacktask.rest.session.validator.GameSessionValidator;
import com.blackjacktask.utilities.Constants;

import java.util.List;

/**
 * Created by apripachkin on 11/18/15.
 */
public class HitOrStandValidator implements GameSessionValidator {

    @Override
    public void validateData(Object... objects) {

        List<String> playerActions = (List<String>) objects[0];
        String action = (String) objects[1];
        if (playerActions == null) {
            throw new IllegalStateException(Constants.INITIALIZE_GAME);
        }
        if (!playerActions.contains(action)) {
            throw new IllegalStateException(Constants.WRONG_REQUEST);
        }

    }
}
