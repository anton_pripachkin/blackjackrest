package com.blackjacktask.rest.session.validator;


/**
 * Created by apripachkin on 11/18/15.
 */
public interface GameSessionValidator {
    void validateData(Object... objects);
}
