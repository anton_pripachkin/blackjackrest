package com.blackjacktask.rest.session.validator.impl;

import com.blackjacktask.rest.model.requests.CreateUserRequest;
import com.blackjacktask.rest.session.validator.GameSessionValidator;
import com.blackjacktask.utilities.Constants;
import com.blackjacktask.utilities.Utils;

import java.util.List;

/**
 * Created by apripachkin on 11/18/15.
 */
public class InitGameValidator implements GameSessionValidator {

    @Override
    public void validateData(Object... objects) {
        long currentId = (long) objects[0];
        if (currentId != -1) {
            throw new IllegalStateException(Constants.GAME_ALREADY_RUNNING);
        }
        List<String> playerActions = (List<String>) objects[1];
        StringBuilder availableActions = new StringBuilder();
        if (playerActions != null) {
            for (String action :
                    playerActions) {
                availableActions.append(action).append(" ");
            }
            throw new IllegalArgumentException("Send request for: " + availableActions);
        }

        CreateUserRequest createUserRequest = (CreateUserRequest) objects[2];
        Utils.checkForNull(Constants.METHOD_PARAMETER_NULL_ERROR, createUserRequest);
        if (createUserRequest.getUserName().length() > 20) {
            throw new IllegalArgumentException("UserName must not be greater than 20 symbols");
        }
    }
}
