package com.blackjacktask.rest.session.validator.impl;

import com.blackjacktask.rest.session.validator.GameSessionValidator;
import com.blackjacktask.utilities.Constants;
import com.blackjacktask.utilities.Utils;

/**
 * Created by apripachkin on 11/18/15.
 */
public class AddFundsValidator implements GameSessionValidator {
    @Override
    public void validateData(Object... objects) {
        long currentId = (long) objects[0];
        if (currentId == - 1) {
            throw new IllegalStateException(Constants.INITIALIZE_GAME);
        }
        double balance = (double) objects[1];
        Utils.checkBalanceValue(balance);
    }
}
