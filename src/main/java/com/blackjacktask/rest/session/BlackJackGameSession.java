package com.blackjacktask.rest.session;

import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.rest.model.requests.CreateUserRequest;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public interface BlackJackGameSession {
    GameState initializeGame(CreateUserRequest createUserRequest);
    GameState startGame(double bet);
    GameState hitRequest();
    GameState standRequest();
    GameState addFunds(double amount);
    GameState deleteUser(String userName);
}
