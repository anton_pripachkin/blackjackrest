package com.blackjacktask.database.impl;

import com.blackjacktask.database.GameUserDbManager;
import com.blackjacktask.database.EntityManagerUtil;
import com.blackjacktask.database.dao.GameUser;
import com.blackjacktask.utilities.Utils;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public class GameUserDbManagerImpl implements GameUserDbManager {
    private EntityManager entityManager = EntityManagerUtil.getEntityManager();

    @Override
    public GameUser saveUser(String name, double initialBalance) {
        Utils.checkForNull(name);
        Utils.checkBalanceValue(initialBalance);
        if (isUserExisting(name)) {
            throw new IllegalArgumentException("You can not create new user with same username " + name);
        }
        GameUser gameUser = new GameUser();
        gameUser.setUserName(name);
        gameUser.setBalance(initialBalance);
        gameUser.setIsPlaying(false);

        entityManager.getTransaction().begin();
        entityManager.persist(gameUser);
        entityManager.getTransaction().commit();
        return gameUser;
    }

    @Override
    public void updateBalance(long id, double balance) {
        entityManager.getTransaction().begin();
        GameUser gameUser = entityManager.find(GameUser.class, id);
        gameUser.setBalance(balance);
        entityManager.getTransaction().commit();
    }

    @Override
    public boolean isUserPlaying(long id) {
        return findUser(id).getIsPlaying();
    }

    private GameUser findUser(long id) {
        entityManager.getTransaction().begin();
        GameUser gameUser = entityManager.find(GameUser.class, id);
        entityManager.getTransaction().commit();
        return gameUser;
    }

    @Override
    public List<GameUser> getUsers() {
        entityManager.getTransaction().begin();
        List<GameUser> gameUsers =
                entityManager.createQuery("SELECT user from GameUser user")
                        .getResultList();
        entityManager.getTransaction().commit();
        return gameUsers;
    }

    @Override
    public GameUser getUser(long id) {
        return findUser(id);
    }

    private boolean isUserExisting(String name) {
        List<GameUser> users = getUsers();
        for (GameUser gameUser : users) {
            if (gameUser.getUserName().equals(name)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void deleteUser(long id) {
        GameUser user = findUser(id);
        entityManager.getTransaction().begin();
        entityManager.remove(user);
        entityManager.getTransaction().commit();
    }

    @Override
    public int removeAllEntries() {
        entityManager.getTransaction().begin();
        int removedEntries = entityManager.createQuery("DELETE from GameUser ").executeUpdate();
        entityManager.getTransaction().commit();
        return removedEntries;
    }

}
