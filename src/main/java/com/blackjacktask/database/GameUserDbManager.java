package com.blackjacktask.database;

import com.blackjacktask.database.dao.GameUser;

import java.util.List;

/**
 * Created by Anton Pripachkin on 14.11.2015.
 */
public interface GameUserDbManager {
    GameUser saveUser(String name, double initialBalance);
    void updateBalance(long id, double balance);
    boolean isUserPlaying(long id);
    List<GameUser> getUsers();
    GameUser getUser(long id);
    void deleteUser(long id);
    int removeAllEntries();
}
