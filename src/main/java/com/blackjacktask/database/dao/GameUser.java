package com.blackjacktask.database.dao;

import com.blackjacktask.utilities.Constants;

import javax.persistence.*;

/**
 * Created by Anton Pripachkin on 14.11.2015.
 */
@Entity
public class GameUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column (name = Constants.COLUMN_ID)
    private Long id;

    @Column (name = Constants.COLUMN_USER_NAME)
    private String userName;

    @Column (name = Constants.COLUMN_IS_PLAYING)
    private Boolean isPlaying;

    @Column(name = Constants.COLUMN_BALANCE)
    private Double balance;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getIsPlaying() {
        return isPlaying;
    }

    public void setIsPlaying(Boolean isPlaying) {
        this.isPlaying = isPlaying;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "GameUser{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", isPlaying=" + isPlaying +
                ", balance=" + balance +
                '}';
    }
}
