package com.blackjacktask.database;

import com.blackjacktask.utilities.Constants;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public class EntityManagerUtil {
    private static final EntityManagerFactory entityManagerFactory;
    static {
        entityManagerFactory = Persistence.createEntityManagerFactory(Constants.PERSISTENCE_UNIT_NAME);
    }
    public static EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
