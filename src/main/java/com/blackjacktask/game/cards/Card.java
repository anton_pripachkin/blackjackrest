package com.blackjacktask.game.cards;

import com.blackjacktask.utilities.Constants;
import com.blackjacktask.utilities.Utils;

/**
 * Created by Anton Pripachkin on 12.11.2015.
 */
public class Card {
    private int rank;
    private CardSuit cardSuit;

    public Card() {
    }

    public Card(int rank, CardSuit cardSuit) {
        checkCardRank(rank);
        setCardSuit(cardSuit);
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        checkCardRank(rank);
    }

    public CardSuit getCardSuit() {
        return cardSuit;
    }

    public void setCardSuit(CardSuit cardSuit) {
        Utils.checkForNull(Constants.METHOD_PARAMETER_NULL_ERROR, cardSuit);
        this.cardSuit = cardSuit;
    }

    private void checkCardRank(int rank) {
        if (rank >= 1 && rank <= 13) {
            this.rank = rank;
        } else {
            throw new IllegalArgumentException("Card rank MUST be equal or less than 13 " +
                    "and equal or greater than 1");
        }
    }

    private String getRankName() {
        String name;
        switch (rank) {
            case 1:
                name = "Ace";
                break;
            case 11:
                name = "King";
                break;
            case 12:
                name = "Jack";
                break;
            case 13:
                name = "Queen";
                break;
            default:
                name = Integer.toString(rank);
        }
        return name;
    }

    @Override
    public String toString() {
        return "Card{" +
                "rank=" + getRankName() +
                ", cardSuit=" + cardSuit +
                '}';
    }
}
