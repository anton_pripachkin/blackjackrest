package com.blackjacktask.game.cards;

/**
 * Created by Anton Pripachkin on 12.11.2015.
 */
public enum CardSuit {
    Clubs,
    Diamonds,
    Hearts,
    Spades
}
