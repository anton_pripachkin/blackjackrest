package com.blackjacktask.game;

import com.blackjacktask.game.cards.Card;
import com.blackjacktask.game.cards.CardSuit;
import com.blackjacktask.utilities.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Anton Pripachkin on 12.11.2015.
 */
public class CardDeck {
    private List<Card> cardList = new ArrayList<>();

    public CardDeck() {
        initCardDeck();
    }

    public void resetCardDeck() {
        cardList.clear();
        initCardDeck();
    }

    private void initCardDeck() {
        for (CardSuit cardSuit : CardSuit.values()) {
            for (int i = 1; i <= Constants.CARDS_PER_SUIT; i++) {
                cardList.add(new Card(i,cardSuit));
            }
        }
        shuffleCards();
    }

    public void setCardList(List<Card> cardList) {
        this.cardList = cardList;
    }

    private void shuffleCards() {
        Collections.shuffle(cardList);
    }

    public Card getCard() {
        return cardList.remove(0);
    }
}
