package com.blackjacktask.game;

import com.blackjacktask.game.cards.Card;
import com.blackjacktask.utilities.Constants;
import com.blackjacktask.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton Pripachkin on 12.11.2015.
 */
public class Hand {
    private List<Card> currentCardsInHand = new ArrayList<>();

    public boolean takeCard(Card card) {
        Utils.checkForNull(Constants.METHOD_PARAMETER_NULL_ERROR, card);
        currentCardsInHand.add(card);
        return getCurrentPoints() <= Constants.WIN_CONDITION;
    }

    public int getCurrentPoints() {
        int cardSum = 0;
        int numOfAces = 0;
        for (Card card : currentCardsInHand) {
            int cardRank = card.getRank();
            if (cardRank == 1) {
                cardSum += Constants.ACE_BLACK_JACK_VALUE;
                numOfAces++;
            } else if (cardRank > 10) {
                cardSum += Constants.HIGH_RANK_VALUE;
            } else {
                cardSum += cardRank;
            }
        }
        while (cardSum > Constants.WIN_CONDITION && numOfAces > 0) {
            cardSum -= 10;
            numOfAces--;
        }
        return cardSum;
    }

    public List<Card> getCurrentCardsInHand() {
        return currentCardsInHand;
    }

    public void resetCardsInHand() {
        currentCardsInHand.clear();
    }

    public boolean isWinConditionMet() {
        return getCurrentPoints() == Constants.WIN_CONDITION;
    }
}
