package com.blackjacktask.game.users;

import com.blackjacktask.game.cards.Card;
import com.blackjacktask.utilities.Constants;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Anton Pripachkin on 12.11.2015.
 */
public class Dealer extends GamePlayer {

    public boolean willHit() {
        return playerHand.getCurrentPoints() < Constants.DEALER_HIT_CONDITION;
    }

    public List<Card> showDealerCards() {
        List<Card> cards = new ArrayList<>(playerHand.getCurrentCardsInHand());
        cards.remove(cards.size() - 1 );
        return cards;
    }

}

