package com.blackjacktask.game.users;

import com.blackjacktask.game.Hand;
import com.blackjacktask.game.cards.Card;

import java.util.List;

/**
 * Created by Anton Pripachkin on 14.11.2015.
 */
public abstract class GamePlayer {
    protected Hand playerHand = new Hand();

    public boolean takeCard(Card card) {
        return playerHand.takeCard(card);
    }
    public List<Card> getGamePlayerCards() {
        return playerHand.getCurrentCardsInHand();
    }
    public void resetCards() {
        playerHand.resetCardsInHand();
    }
    public boolean isWinConditionMet() {return playerHand.isWinConditionMet();}
    public int getCardSum() {return playerHand.getCurrentPoints();}
}
