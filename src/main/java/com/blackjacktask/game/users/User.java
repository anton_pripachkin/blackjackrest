package com.blackjacktask.game.users;

/**
 * Created by Anton Pripachkin on 12.11.2015.
 */
public class User extends GamePlayer {
    private long userId;
    private String userName;
    private Double accountBalance;

    public User(long userId, String userName, Double accountBalance) {
        this.userId = userId;
        this.userName = userName;
        this.accountBalance = accountBalance;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(Double accountBalance) {
        this.accountBalance = accountBalance;
    }
}
