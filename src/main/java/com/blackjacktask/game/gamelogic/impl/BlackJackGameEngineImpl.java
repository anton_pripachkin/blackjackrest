package com.blackjacktask.game.gamelogic.impl;

import com.blackjacktask.database.dao.GameUser;
import com.blackjacktask.game.CardDeck;
import com.blackjacktask.game.cards.Card;
import com.blackjacktask.game.cards.CardSuit;
import com.blackjacktask.game.gamelogic.BlackJackGameEngine;
import com.blackjacktask.game.gamelogic.RoundResult;
import com.blackjacktask.game.users.Dealer;
import com.blackjacktask.game.users.GamePlayer;
import com.blackjacktask.game.users.User;
import com.blackjacktask.rest.model.GameState;
import com.blackjacktask.utilities.Constants;
import com.blackjacktask.utilities.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public class BlackJackGameEngineImpl implements BlackJackGameEngine {
    private User user;
    private double bet;
    private Dealer dealer = new Dealer();
    private CardDeck cardDeck = new CardDeck();
    private GameState gameState = new GameState();
    private List<Card> cheatCards = new ArrayList<>();

    @Override
    public GameState initGame(GameUser gameUser) {
        Utils.checkForNull(Constants.METHOD_PARAMETER_NULL_ERROR, gameUser);
        user = new User(gameUser.getId(), gameUser.getUserName(), gameUser.getBalance());
        gameState.setGameStatus("User with name: " + user.getUserName() + " created");
        gameState.setUserBalance(user.getAccountBalance());
        return gameState;
    }

    @Override
    public GameState hit() {
        Utils.checkForNull(Constants.INITIALIZE_GAME, user);
        if (user.getCardSum() <= Constants.WIN_CONDITION) {
            return userHit();
        }
        return endGame(calculateRoundResult(user, dealer));
    }


    @Override
    public GameState stand() {
        Utils.checkForNull(Constants.INITIALIZE_GAME, dealer);
        while (dealer.willHit()) {
            dealer.takeCard(cardDeck.getCard());
        }
        return endGame(calculateRoundResult(user, dealer));
    }

    @Override
    public GameState startGame(double bet) {
        Utils.checkForNull(Constants.INITIALIZE_GAME, user, dealer);
        initPlayers(user, dealer);
        this.bet = bet;
        user.setAccountBalance(user.getAccountBalance() - bet);
        RoundResult roundResult = gameEndOnBlackJack(user, dealer);
        if (roundResult != null) {
            return endGame(roundResult);
        }
        List<String> playerActions = new ArrayList<>();
        playerActions.add(Constants.ACTION_HIT);
        playerActions.add(Constants.ACTION_STAND);
        gameState.setGameStatus(Constants.GAME_STARTED);
        gameState.setPlayerActions(playerActions);
        gameState.setUserBalance(user.getAccountBalance());
        gameState.setDealerCards(dealer.showDealerCards());
        gameState.setUserCards(user.getGamePlayerCards());
        return gameState;
    }

    private GameState endGame(RoundResult roundResult) {
        double win = calculateWin(bet, roundResult);
        user.setAccountBalance(user.getAccountBalance() + win);
        GameState endGameState = getEndGameState(roundResult);
        resetGame();
        return endGameState;
    }

    @Override
    public void updateBalance(double balance) {
        Utils.checkForNull(Constants.INITIALIZE_GAME, gameState, user);
        gameState.setUserBalance(balance);
        user.setAccountBalance(balance);
    }

    @Override
    public void resetBlackJackGame() {
        resetGame();
        user = null;
    }

    private GameState userHit() {
        if (!user.takeCard(cardDeck.getCard())) {
            return endGame(calculateRoundResult(user, dealer));
        } else {
            gameState.setUserCards(user.getGamePlayerCards());
            return gameState;
        }
    }

    private GameState getEndGameState(RoundResult roundResult) {
        GameState gameState = new GameState();
        ArrayList<String> strings = new ArrayList<>();
        strings.add(Constants.START_NEW_ROUND);
        ArrayList<Card> userCards = new ArrayList<>(user.getGamePlayerCards());
        ArrayList<Card> dealerCards = new ArrayList<>(dealer.getGamePlayerCards());
        gameState.setUserCards(userCards);
        gameState.setPlayerActions(strings);
        gameState.setDealerCards(dealerCards);
        gameState.setUserBalance(user.getAccountBalance());
        gameState.setGameStatus(Constants.ROUND_END);
        gameState.setRoundResult(roundResult);
        return gameState;
    }

    private void resetGame() {
        cardDeck.resetCardDeck();
        dealer.resetCards();
        user.resetCards();
        gameState = new GameState();
    }

    private RoundResult calculateRoundResult(GamePlayer user, GamePlayer dealer) {
        int userCardSum = user.getCardSum();
        int dealerCardSum = dealer.getCardSum();
        if (userCardSum > Constants.WIN_CONDITION || (userCardSum < dealerCardSum && dealerCardSum <= Constants.WIN_CONDITION)) {
            return RoundResult.LOSE;
        } else if (dealerCardSum > Constants.WIN_CONDITION || dealerCardSum < userCardSum) {
            return RoundResult.WIN;
        } else {
            return RoundResult.DRAW;
        }
    }

    private double calculateWin(double bet, RoundResult roundResult) {
        double winAmount = 0;
        switch (roundResult) {
            case WIN:
                winAmount = bet * 1.5 + bet;
                break;
            case DRAW:
                winAmount = bet;
        }
        return winAmount;
    }

    private RoundResult gameEndOnBlackJack(GamePlayer user, GamePlayer dealer) {
        RoundResult roundResult = null;
        if (isBlackJackPush(user, dealer)) {
            roundResult = RoundResult.DRAW;
        } else if (user.isWinConditionMet()) {
            roundResult = RoundResult.WIN;
        } else if (dealer.isWinConditionMet()) {
            roundResult = RoundResult.LOSE;
        }
        return roundResult;
    }

    private boolean isBlackJackPush(GamePlayer user, GamePlayer dealer) {
        return user.isWinConditionMet() && dealer.isWinConditionMet();
    }

    private void initPlayers(GamePlayer user, GamePlayer dealer) {
        takeCards(user);
        takeCards(dealer);
    }

    private void takeCards(GamePlayer user) {
        for (int i = 0; i < 2; i++) {
            user.takeCard(cardDeck.getCard());
        }
    }

    public void cheatCustomCards(List<Card> cardList) {
        cardDeck.setCardList(cardList);
    }

    public GameState cheatPlayerBlackJack() {
        cheatCards.add(new Card(1, CardSuit.Clubs));
        cheatCards.add(new Card(10,CardSuit.Clubs));
        cheatCards.add(new Card(2,CardSuit.Clubs));
        cheatCards.add(new Card(3,CardSuit.Clubs));
        return initCheat();
    }

    public GameState cheatPush() {
        cheatCards.add(new Card(1, CardSuit.Clubs));
        cheatCards.add(new Card(10,CardSuit.Clubs));
        cheatCards.add(new Card(1,CardSuit.Diamonds));
        cheatCards.add(new Card(10,CardSuit.Spades));
        return initCheat();
    }

    public GameState cheatDealerBlackJack() {
        cheatCards.add(new Card(2,CardSuit.Clubs));
        cheatCards.add(new Card(3,CardSuit.Clubs));
        cheatCards.add(new Card(1, CardSuit.Clubs));
        cheatCards.add(new Card(10,CardSuit.Clubs));
        return initCheat();
    }

    public GameState cheatUserWinStand() {
        cheatCards.add(new Card(2,CardSuit.Clubs));
        cheatCards.add(new Card(3,CardSuit.Clubs));
        cheatCards.add(new Card(10,CardSuit.Clubs));
        cheatCards.add(new Card(6,CardSuit.Clubs));
        cheatCards.add(new Card(10,CardSuit.Diamonds));
        return initCheat();
    }

    private GameState initCheat() {
        cardDeck.setCardList(new ArrayList<Card>(cheatCards));
        cheatCards.clear();
        return startGame(20);
    }

}