package com.blackjacktask.game.gamelogic;

import com.blackjacktask.database.dao.GameUser;
import com.blackjacktask.rest.model.GameState;

/**
 * Created by apripachkin on 11/16/15.
 */
public interface BlackJackGameEngine {
    GameState initGame(GameUser gameUser);
    GameState startGame(double bet);
    GameState hit();
    GameState stand();
    void updateBalance(double balance);
    void resetBlackJackGame();
}
