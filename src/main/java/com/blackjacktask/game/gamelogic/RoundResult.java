package com.blackjacktask.game.gamelogic;

/**
 * Created by Anton Pripachkin on 15.11.2015.
 */
public enum RoundResult {
    LOSE,
    WIN,
    DRAW
}
