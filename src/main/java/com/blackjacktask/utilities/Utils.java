package com.blackjacktask.utilities;

import com.blackjacktask.rest.model.GameState;

/**
 * Created by apripachkin on 11/13/15.
 */
public class Utils {

    public static void checkForNull(String message, Object... objects) {
        for (Object obj :
                objects) {
            if (obj == null) {
                throw new IllegalArgumentException(message);
            }
        }
    }

    public static void checkBalanceValue(double balance) {
        if (balance <= 0.0) {
            throw new IllegalArgumentException(Constants.WRONG_BALANCE_VALUE);
        }
    }

    public static boolean checkBetValues(double balance, double bet) {
        return bet <= balance && bet > 0;
    }

    public static GameState handleGameError(String error) {
        GameState gameState = new GameState();
        gameState.setGameStatus(error);
        return gameState;
    }
}
