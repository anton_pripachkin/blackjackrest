package com.blackjacktask.utilities;

/**
 * Created by apripachkin on 11/12/15.
 */
public final class Constants {
    //Messages
    public static final String PLEASE_ADD_BALANCE = "Please add balance";
    public static final String GAME_ALREADY_RUNNING = "Game is already in progress, you can't start a new game";
    public static final String GAME_STARTED = "Game Started!";
    public static final String HIT_OR_STAY = "Hit or Stay?";
    public static final String NEED_TO_START_GAME = "You need to start game first";
    public static final String METHOD_PARAMETER_NULL_ERROR = "Method parameter must not be null!";
    public static final String BALANCE_UPDATED = "Balance updated!";
    public static final String INITIALIZE_GAME = "You need to initialize game first";
    public static final String ROUND_END = "Round ended";
    public static final String WRONG_REQUEST = "Wrong request";
    public static final String DELETE_ACTIVE_USER = "You can't delete a user that's currently playing";
    public static final String USER_DELETED = "User deleted!";
    //Database
    public static final String PERSISTENCE_UNIT_NAME = "manager";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USER_NAME = "UserName";
    public static final String COLUMN_BALANCE = "balance";
    public static final String COLUMN_IS_PLAYING = "isPlaying";
    public static final String WRONG_BALANCE_VALUE = "Incorrect balance value";
    //Number Conditions
    public static final int DEALER_HIT_CONDITION = 17;
    public static final int WIN_CONDITION = 21;
    public static final int HIGH_RANK_VALUE = 10;
    public static final int ACE_BLACK_JACK_VALUE = 11;
    public static final int CARDS_PER_SUIT = 13;
    //Paths
    public static final String PATH_STAND = "stand";
    public static final String PATH_HIT = "hit";
    public static final String PATH_DELETE = "delete";
    public static final String PATH_CREATE_ACCOUNT = "createAccount";
    public static final String PATH_START_GAME = "startgame";
    public static final String PATH_ADD_FUNDS = "addFunds";
    //Player actions:
    public static final String ACTION_HIT = "hitAction";
    public static final String ACTION_STAND = "standAction";
    public static final String START_NEW_ROUND = "Start a new round";

    public static final String TEST_USERNAME = "TestUser";

}
