
### BlackJack RESTful game ###

* Sample blackJack game made with java EE technologies

### Project description ###

* Send a json post request (sampe in project resources) to /webapi/createAccount with initial username and account balance. Game will send a json response
* Send a post request to /webapi/startgame/{bet} to start a new round
* Send a post request to /webapi/addFunds/{amount} to add balance to current user
* Send a post request to /webapi/delete/{userName} to delete current user after round end
* Send a post request to /webapi/hit or /webapi/stand to send a hit or stay request